<?php

namespace sgb004\OrangeLeadsTest\Controller;

use Symfony\Component\Form\Form;
use sgb004\OrangeLeadsTest\Entity\Lead;
use sgb004\OrangeLeadsTest\Form\LeadsType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class LeadsController extends AbstractController
{
	public function __construct(KernelInterface $kernel){
		$this->rootDir = $kernel->getProjectDir();
	}
	
	protected function getLeadsTypeClass(){
		return LeadsType::class;
	}

	protected function getLeadClass(){
		return new Lead();
	}

	/**
	 * @Route("/leads/add", name="leads_add")
	 */
	public function add(Request $request, \Swift_Mailer $mailer): Response
	{
		$r = ['success' => false, 'notice' => 'Please, send data.', 'fields' => array()];
		$putData = json_decode($request->getContent(), true);

		if( is_array($putData) ){
			$r['notice'] = 'Check errors in fields.';
			$lead = $this->getLeadClass();
			$form = $this->createForm($this->getLeadsTypeClass(), $lead);
			
			$form->submit($putData);

			if($form->isSubmitted() && $form->isValid()){
				$children = $form->all();
				foreach ($children as $name => $child) {
					$data[$name] = $child->getData();
				}
				$this->sendLeadMail($data, $mailer);
				$this->sendAdminMail($data, $mailer);
				
				$lead = $form->getData();
				$lead->autoRegisterDate();
				$entityManager = $this->getDoctrine()->getManager();
				$entityManager->persist($lead);
				$entityManager->flush();
				
				$r = ['success' => false, 'notice' => $lead->getId()];
			}else{
				$formError = $form->getErrors()->current();
				$formError = is_object($formError) ? $formError->getMessage() : '';

				if( $formError == '' ){
					$children = $form->all();
					foreach ($children as $name => $child) {
						$childError = $child->getErrors()->__toString();
						if($childError == '' ){continue;}
						$r['fields'][$name] = explode("\n", trim(str_replace('ERROR: ', '', $childError)));
					}
				}else{
					$r['notice'] = $formError;
				}
			}
		}

		return $this->json($r);
	}

	protected function sendLeadMail(array $data, \Swift_Mailer $mailer){
		$message = (new \Swift_Message('Thank you for contact us!'))
		->setFrom($this->getParameter('app.sender_mail'), $this->getParameter('app.sender_mail_name'))
		->setTo($data['email'], $data['name'])
		->setBody($this->replaceDataFile('templates/GAcertification/mailing/lead.twig', $data), 'text/html');
		$mailer->send($message);
	}

	protected function sendAdminMail(array $data, \Swift_Mailer $mailer){
		$message = (new \Swift_Message('A new user was registered'))
		->setFrom($this->getParameter('app.sender_mail'), $this->getParameter('app.sender_mail_name'))
		->setTo($this->getParameter('app.sender_mail_admin'), $this->getParameter('app.sender_mail_admin_name'))
		->setBody($this->replaceDataFile('templates/GAcertification/mailing/admin.twig', $data), 'text/html');
		$mailer->send($message);
	}

	protected function replaceDataFile(string $file, array $data){
		$content = file_get_contents($this->rootDir.'/'.$file);
		foreach ($data as $key => $value) {
			$content = str_replace('{{ '.$key.' }}', $value, $content);
		}
		return $content;
	}

	/**
	 * @Route("/send_test", name="send_test")
	 */
	/*
	public function sendTest(Form $form, \Swift_Mailer $mailer){
		$message = (new \Swift_Message('Prueba de envío'))
		->setFrom($this->getParameter('app.sender_mail'), $this->getParameter('app.sender_mail_name'))
		->setTo('sgb_004@yahoo.com.mx', 'sgb')
		->setBody(
			'<p>Prueba!</p>',
			'text/html'
		);

		$r = 0;

		$r = $mailer->send($message);

		return $this->json(array('result' => $r));
	}
	*/
}
