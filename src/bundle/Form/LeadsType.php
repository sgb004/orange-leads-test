<?php

namespace sgb004\OrangeLeadsTest\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;

class LeadsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
			->add('name', TextType::class, [
				'constraints' => [
					new NotBlank()
				]
			])
			->add('last_name', TextType::class, [
				'constraints' => []
			])
			->add('email', EmailType::class, [
				'constraints' => [
					new NotBlank()
				]
			])
			->add('phone', NumberType::class, [
				'constraints' => [
					new NotBlank()
				]
			])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}
